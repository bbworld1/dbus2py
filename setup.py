import setuptools
import os
import sys

setuptools.setup(
    name="dbus2py",
    author="Vincent Wang",
    packages=setuptools.find_packages(),
    scripts=["scripts/dbus2py"]
)
