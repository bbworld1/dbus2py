# dbus2py - generate Python bindings to any DBus service! #
dbus2py leverages the availability of the
`org.freedesktop.DBus.Introspection` interface to generate
Python wrapper classes for any DBus service. This makes it
much easier to access DBus APIs in a Pythonic way.

## Requirements ##
To run dbus2py, all you need are dbus-python, jinja2 and Python3+.

## Usage ##
To use dbus2py:
`dbus2py "com.example.ExampleService"`
where com.example.ExampleService is the service
you want to generate a wrapper for.
The resulting Python wrapper classes will be printed to stdout
or stored in the specified Python file.

Use `dbus2py -h` for a full help list.

NOTE: dbus2py uses a very simple object path guessing algorithm
by default. For more accurate results, specify the object path
after the service name.

## Support ##
dbus2py currently generates accurate method calls, getters/setters
for properties, and add_listener methods for signals. You can always
modify the generated classes to add functionality, fix inaccuracies,
or change the name.

Do you want to add a feature to dbus2py? Submit an issue and/or
create a merge request.

## Installation ##
Clone this repo.
`python3 setup.py install` (you may need sudo).

## Planned Features ##
- Type annotations for input and output
- Support async etc.
- Better object path guessing
