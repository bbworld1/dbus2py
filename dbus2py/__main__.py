from dbus2py import opts
from dbus2py import introspector
from dbus2py import parser
from dbus2py import renderer

options = opts.get_args()
bus_type, object_path, data = introspector.introspect(options.service, options.object_path)
parsed_data = parser.parse(data)
rendered = renderer.render(parsed_data, bus_type, options.service, object_path)
if options.outputfile:
    with open(options.outputfile, "w") as f:
        f.write(rendered)
else:
    print(rendered)
