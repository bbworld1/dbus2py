from jinja2 import Environment, PackageLoader

def render(parsed_data, bustype, service, obj_path):
    interfaces = parsed_data
    interface_names = list(map(lambda x: camelcase(x),
                               interfaces.keys()))

    env = Environment(loader=PackageLoader("dbus2py", "templates"))
    template = env.get_template("class_template.py")

    rendered = template.render(interfaces=interfaces,
                               interface_names=interface_names,
                               bustype=bustype,
                               service=service,
                               obj_path=obj_path)
    return rendered

def camelcase(name):
    """
    Takes an interface name with dots and stuff
    and converts it to CamelCase.
    """
    camel = ""
    upper = True
    for c,l in enumerate(name):
        if l == ".":
            upper = True
            continue

        if upper:
            camel += l.upper()
        else:
            camel += l

        upper = False

    return camel
