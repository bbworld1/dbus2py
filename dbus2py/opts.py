import argparse

def get_args():
    parser = argparse.ArgumentParser(prog="dbus2py",
                                     description='Generate Python class wrappers for DBus interfaces.')
    parser.add_argument("-o", "--outputfile", help="File to output to (default: print to stdout)")
    parser.add_argument("service", help="Service to generate a wrapper for")
    parser.add_argument("object_path",
                        nargs="?",
                        help="Object path to generate a wrapper for (default: guess based on service name)")
    return parser.parse_args()
