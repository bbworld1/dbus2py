import dbus
import xml.etree.ElementTree as ET

def introspect(service, obj=None):
    if obj == None:
        obj = "/" + service.replace(".", "/")

    try:
        bus = dbus.SessionBus()
        return "SessionBus", obj, _introspect(service, obj, bus)
    except dbus.exceptions.DBusException as e:
        if e.get_dbus_name() == "org.freedesktop.DBus.Error.ServiceUnknown":
            # It wasn't in the session bus, look in system bus
            pass
        else:
            raise e

    try:
        bus = dbus.SystemBus()
        return "SystemBus", obj, _introspect(service, obj, bus)
    except dbus.exceptions.DBusException as e:
        if e.get_dbus_name() == "org.freedesktop.DBus.Error.ServiceUnknown":
            print("E: Service not found!")
        else:
            raise e

def _introspect(service, obj, bus):
    proxy = bus.get_object(service, obj)
    iface = dbus.Interface(proxy,
                           dbus_interface="org.freedesktop.DBus.Introspectable")
    return ET.fromstring(iface.Introspect())
