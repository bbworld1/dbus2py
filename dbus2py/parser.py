# BEWARE: Here lie dragons
# This file contains a stupid amount of nested logic
# and it doesn't look like there's really a way to un-nest it
# If you have an idea for how to do so please submit a MR
import xml.etree.ElementTree as ET

def parse(data):
    # Generates a parsed dictionary in the form
    # {interface: [{name: name, type: method/property, more: data}]}
    parsetree = {}
    for interface in data:
        parsetree[interface.attrib["name"]] = []
        for mop in interface:
            mop_dict = {"name": mop.attrib["name"],
                        "pythonized_name": pythonize_name(mop.attrib["name"]),
                        "property_or_method": mop.tag}
            if mop.tag == "method":
                mop_dict["args"] = []
                for arg in mop:
                    # add args
                    if arg.tag != "arg":
                        # Ignore annotations etc
                        # TODO: Implement annotations etc
                        continue

                    if arg.attrib["direction"] == "out":
                        # This returns something
                        mop_dict["returns"] = True
                    else:
                        # Doesn't return anything
                        mop_dict["returns"] = False

                    arg_dict = {"type": arg.attrib["type"],
                                "direction": arg.attrib["direction"],
                                "name": arg.attrib["name"],
                                "pythonized_name": pythonize_name(arg.attrib["name"])}
                    mop_dict["args"].append(arg_dict)
            elif mop.tag == "property":
                mop_dict.update({"access": mop.attrib["access"],
                                 "type": mop.attrib["type"]})

            parsetree[interface.attrib["name"]].append(mop_dict)

    return parsetree

def pythonize_name(name):
    # Convert a name to snake_case
    newname = ""
    for c, l in enumerate(name):
        if c == 0:
            newname += l.lower()
            continue

        if l.isupper():
            newname += "_"
            newname += l.lower()
        else:
            newname += l

    return newname

