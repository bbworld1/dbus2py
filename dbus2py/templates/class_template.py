import dbus

{% for interface_name, interface in interfaces.items() %}
class {{interface_names[loop.index-1]}}:
    def __init__(self):
        self.bus = dbus.{{ bustype }}()
        self.proxy = self.bus.get_object('{{ service }}',
                                          '{{ obj_path }}')
        self.iface = dbus.Interface(self.proxy,
                                    dbus_interface='{{ interface_name }}')
        self.properties = dbus.Interface(self.proxy,
                                         dbus_interface="org.freedesktop.DBus.Properties")
    {%- for mop in interface %}
    {%- if mop["property_or_method"] == "method" %}

    def {{mop["pythonized_name"]}}(self{% for arg in mop["args"] %}{% if arg["direction"] == "in" %}, {{ arg["pythonized_name"] }}{% endif %}{%- endfor %}):
        {% if mop["returns"] %}return {% endif %}self.iface.{{mop["name"]}}({% for arg in mop["args"] %}{% if arg["direction"] == "in" %}{% if not loop.first %}, {% endif %}{{ arg["pythonized_name"] }}{% endif %}{%- endfor %})
    {%- elif mop["property_or_method"] == "property" %}
    {% if mop["access"] in ["read", "readwrite"] %}
    def get_{{mop["pythonized_name"]}}(self):
        return self.properties.Get('{{interface_name}}',
                                   '{{mop["name"]}}')

    {% endif %}
    {%- if mop["access"] in ["write", "readwrite"] %}
    def set_{{mop["pythonized_name"]}}(self, value):
        self.properties.Set('{{interface_name}}',
                            '{{mop["name"]}}',
                            value)

    {%- endif %}
    {%- elif mop["property_or_method"] == "signal" %}
    def add_listener_{{mop["pythonized_name"]}}(self, listener):
        """
        Add a callback function as a listener for this event.
        """
        self.iface.connect_to_signal('{{mop["name"]}}',
                                     listener)
    {%- endif %}
    {%- endfor %}

{% endfor %}
